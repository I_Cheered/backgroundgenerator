#pragma once
#include "resources.hpp"

void movePoints(point **pointArray);
void drawTriangles_grad(triangle **triangleArray, sf::RenderWindow &(window));
void drawTriangles_img(triangle **triangleArray, sf::RenderWindow &(window), sf::Image* img);
void changeColor();
